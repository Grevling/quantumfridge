export function getCookie(cname) {
    let name = cname + "=";
    let decodedCookie = document.cookie;
    let ca = decodedCookie.split(';');
    for(let i = 0; i <ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) === ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}


export function setCookie(cname, cvalue, exdays) {
    let d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    let expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

export function deleteCookie( name ) {
    document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}

export function download(filename, text) {
    let pom = document.createElement('a');
    pom.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    pom.setAttribute('download', filename);

    if (document.createEvent) {
        var event = document.createEvent('MouseEvents');
        event.initEvent('click', true, true);
        pom.dispatchEvent(event);
    }
    else {
        pom.click();
    }
}

export function percentageString(a, b){
    return "" + ((a / b) * 100) + "%";
}

export function getArrayOfOnes(numberOfOnes) {
    let array = [];
    for(let i = 0; i < numberOfOnes; i++){
        array[i] = 1;
    }
}

export function fillArray(numberOfOnes) {
    return Array(numberOfOnes).fill(1);
}

export function getArrayOfConsecutiveIntegersFromOneTo(numberOfIntegers){
    let returnArray = [];
    for (let i = 0; i < numberOfIntegers; i++) {
        returnArray.push(i+1);
    }
    return returnArray;
}

export const noiseUrls = {
    oneCity: 'https://firebasestorage.googleapis.com/v0/b/quantumfridge.appspot.com/o/One%20City_v6.mp3?alt=media&token=2d488330-6c2b-42c8-8541-12120c90592f',
    mtc: 'https://firebasestorage.googleapis.com/v0/b/quantumfridge.appspot.com/o/MTC.mp3?alt=media&token=2eeda12e-6818-42f5-83eb-6909f3a77bcb',
    elektronik: 'https://firebasestorage.googleapis.com/v0/b/quantumfridge.appspot.com/o/elektronik_3.mp3?alt=media&token=be74950f-796f-4eb9-9c93-f616f3f38b2c',
    dataFrame: 'https://firebasestorage.googleapis.com/v0/b/quantumfridge.appspot.com/o/data%20frame_v1.mp3?alt=media&token=9e1fc5b6-5e4d-4922-809e-797e0984e55d',
    guatemalanBeeherders: 'https://firebasestorage.googleapis.com/v0/b/quantumfridge.appspot.com/o/guatemalan%20beeherders.mp3?alt=media&token=b4e577b9-5429-44df-90a0-abb3e30cdffb',
    depressionOfTheTriangularButton: 'https://firebasestorage.googleapis.com/v0/b/quantumfridge.appspot.com/o/Depression_of_the_Triangular_Button.mp3?alt=media&token=76c73975-9fd5-41c1-9da6-2790f1a9e66c',
    p88: 'https://firebasestorage.googleapis.com/v0/b/quantumfridge.appspot.com/o/p88%20v4.mp3?alt=media&token=e10c04a9-36e5-44e7-9298-364a1fa735d4',
};

export function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

export default this;