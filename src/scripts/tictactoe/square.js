import React from "react";

function Square(props) {
    if(props.winner) {
        return (
            <button className="square winner" onClick={props.onClick}>
                {props.value}
            </button>
        );
    }

    return (
        <button className="square" onClick={props.onClick}>
            {props.value}
        </button>
    );
}
export default Square;