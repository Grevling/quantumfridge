
function winconditions(numcol){
    var lines = [];
    // horizontal
    for (var i = 0; i < numcol; i++) {
        let rowStart = numcol * (i);
        for (var j = 0; j < numcol - 2; j++) {
            let first = rowStart + j;
            lines[lines.length] = [first, first + 1, first + 2];
        }
    }
    // vertical
    for (var i1 = 0; i1 < numcol - 2; i1++) {
        let rowStart = numcol * (i1);
        for (var j1 = 0; j1 < numcol; j1++) {
            let one = rowStart + j1;
            let two = one + numcol;
            let three = two + numcol;

            lines[lines.length] = [one, two, three];
        }
    }
    // diagonal left to right
    for (var i2 = 0; i2 < numcol - 2; i2++) {
        let rowStart = numcol * (i2);
        for (var j2 = 0; j2 < numcol - 2; j2++) {
            let one = rowStart + j2;
            let two = one + numcol + 1;
            let three = two + numcol + 1;
            if (three > (numcol * numcol) - 1) {
                break;
            }
            lines[lines.length] = [one, two, three];
        }
    }
    // diagonal right to left
    for (var i3 = 0; i3 < numcol - 2; i3++) {
        let rowStart = numcol * (i3);
        for (var j3 = 2; j3 < numcol; j3++) {
            let one = rowStart + j3;
            let two = one + numcol - 1;
            let three = two + numcol - 1;
            lines[lines.length] = [one, two, three];
        }
    }
    return lines;
}

export default winconditions;