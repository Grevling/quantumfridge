import React from "react";
import '../../style/Dungeoneer/clippath.css';



class ClipPathPicture extends React.Component {
    constructor(props) {
        super(props);
        // this.state = this.props.state
    }

    // componentDidMount(){
    //     // console.log('didmount');
    // }

    getThings(numThings){
        let returnArray = [];
        for(let i = 0; i < numThings; i++) {
            returnArray.push(<div className={"clipDiv cd" + i}/>)
        }
        return returnArray;
    }

    render(){
        return(
            <div className='clipPathContent'>
                {this.getThings(28)}
                <div className='genericExitDivLeft' onClick={() => this.props.move('town')}/>


                <clipPath id="myClip">
                    <circle cx="100" cy="100" r="40"/>
                    <circle cx="60" cy="60" r="40"/>
                </clipPath>
            </div>
        );
    }
}

export default ClipPathPicture;