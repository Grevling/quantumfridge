import React from "react";
// import {Button, Clearfix} from 'react-bootstrap';
import SuffleGame from './shufflegame/shufflegame';
import '../../style/Dungeoneer/world.css';
import '../../style/Dungeoneer/shuffleGame.css';
import '../../style/Dungeoneer/church.css';

class Church extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            playingSuffleGame: false
        };
        this.stopShuffling = this.stopShuffling.bind(this);
        this.resurrect = this.resurrect.bind(this);
        // debugger
    }

    resurrect(priestRess){
        if (this.props.state.dead) {
            let glodz = this.props.state.glodz;
            if (priestRess) {
                glodz = Math.floor(glodz/2);
            }
            this.props.saveState({
                infoText: 'Ragnar lives once more! Will he have revenge as well?',
                dead: false,
                ragnarHealth: this.props.state.ragnarMaxHealth,
                glodz: glodz
            });
        }
    }

    stopShuffling(){
        this.setState({playingSuffleGame: false});
    }

    render(){
        let shuffleThing = this.state.playingSuffleGame ?
            <SuffleGame resurrect={this.resurrect} stopShuffling={this.stopShuffling} /> :
            <div className='shuffleGameStart' onClick={() => this.setState({playingSuffleGame: true})}/>;
            {/*<div className={this.props.state.dead ? 'shuffleGameStart' : 'meh'} onClick={() => this.setState({playingSuffleGame: true})}/>;*/}
        return(
            <div className='churchContent'>
                <div className='genericExitDivLeft' onClick={() => this.props.move('town')}/>
                <div className={this.props.state.dead ? 'priestDiv glow' : 'meh'} onClick={() => this.resurrect(true)}/>
                {shuffleThing}
            </div>
        );
    }
}

export default Church;