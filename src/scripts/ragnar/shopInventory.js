import React from "react";
import ReactFitText from 'react-fittext';
import {Clearfix} from 'react-bootstrap';
import '../../style/Dungeoneer/shop.css';

class ShopInventory extends React.Component {
    constructor(props) {
        super(props);
        this.state = this.props.state;
    }


    componentDidMount(){
        this.setState(this.props.state);
    }


    saveState(state){
        // console.log('saving state:');
        // console.log(state);
        this.setState(state);
        this.props.saveState(state);
    }

    getSliderSpot(value){
        let numberOfItems = this.state.items.length;
        let sliderSpot = this.state.sliderSpot;
        let newSlideValue = sliderSpot + value;
        if(newSlideValue > numberOfItems){
            newSlideValue = 1;
        }else if(newSlideValue < 1) {
            newSlideValue = numberOfItems;
        }
        return newSlideValue;
    }

    slide(value){
        let numberOfItems = this.state.items.length;
        let sliderSpot = this.state.sliderSpot;
        let newSlideValue = sliderSpot + value;
        if(newSlideValue > numberOfItems){
            newSlideValue = 1;
        }else if(newSlideValue < 1) {
            newSlideValue = numberOfItems;
        }
        this.saveState({sliderSpot: newSlideValue});
    }

    buy(index){
        let item = this.state.items[index];
        let pocket = this.state.ragnarsPocket;
        if (this.state.glodz < item.price){
            //can't afford
            console.log("can't afford " + item.type);
        } else {
            console.log("tryna buy");
            let itemType = item.type;
            let prevValue = pocket[itemType + 's'];
            pocket[itemType + 's'] = prevValue + 1;
            let stateUpdate = {};
            stateUpdate['glodz'] = this.state.glodz - item.price;
            stateUpdate['vendorGlodz'] = this.state.vendorGlodz + item.price;
            stateUpdate['ragnarsPocket'] = pocket;
            console.log(stateUpdate);
            this.saveState(stateUpdate);
        }
    }

    trySell(object){
        // console.log("test thing:");
        //
        // let i = 10;
        // while (i--){
        //     console.log(i);
        // }

        let price = object.price;
        let vendorMoneyAfterTransaction = this.state.vendorGlodz - price;
        let playerMoneyAfterTransaction = this.state.glodz + price;
        if (vendorMoneyAfterTransaction >= 0 && playerMoneyAfterTransaction >= 0){
            let index = this.state.epix.indexOf(object);
            this.state.epix.splice(index, 1);
            this.saveState({
                vendorGlodz: vendorMoneyAfterTransaction,
                glodz: playerMoneyAfterTransaction
            });
        }
    }

    getEpix(){
        let epixObjects = this.state.epix;
        let returnArray = [];
        for(let i = 0; i < 5; i++){

            let epixToReturn = <div className="emptyEpixSlot"/>;
            if (epixObjects[i]){
                let epixObj = epixObjects[i];
                let name = epixObj.name;
                let price = epixObj.price;
                let description = epixObj.description;
                epixToReturn = <div className="epix" key={i} title={description} onClick={() => this.trySell(epixObj)}>
                    <ReactFitText compressor={0.6}>
                        <p className="epixName">
                            {name}
                        </p>
                    </ReactFitText>
                    <ReactFitText compressor={1}>
                        <p>
                            "Worth": {price}
                        </p>
                    </ReactFitText>

                </div>
            }

            returnArray[i] = epixToReturn;
        }
        return returnArray;
    }

    getItem(thing, compressorThing){
        let index = this.getSliderSpot(thing -1)-1;

        return(
        <div className={'itemDiv item' + (thing + 1)} title={this.state.items[index].name} onClick={() => this.buy(index)}>
            <div id={'item' + (index + 1)} className='itemPictureDiv'/>
            <ReactFitText compressor={compressorThing}>
                <p>{this.state.items[index].price + ' Glodz'}</p>
            </ReactFitText>
            <ReactFitText compressor={compressorThing}>
                <p>Got: {this.getNumberOfItems(index)}</p>
            </ReactFitText>
        </div>
        );
    }

    getItems(compressorThing){
        let returnArray = [];
        for (let i = 0; i < 3; i++) {
            returnArray[i] = this.getItem(i, compressorThing);
        }
        return returnArray;
    }

    getNumberOfItems(index){
        let item = this.state.items[index];
        let type = item.type;
        let key = type + 's';
        let items = this.state.ragnarsPocket;
        return items[key];
    }


    render(){
        let compressorThing = 0.55;

        return(
            <div className='shopInventoryContent'>
                <div className='itemsDiv'>
                    <div className='itemSliderButton goLeftDiv' onClick={() => this.slide(-1)}/>
                    {this.getItems(compressorThing)}
                    <div className='itemSliderButton goRightDiv' onClick={() => this.slide(1)}/>
                </div>
                <Clearfix/>
                <div className="epixDiv">
                    {this.getEpix()}
                </div>
                <div className='glodzDiv moneybagDiv'>
                    <ReactFitText compressor={compressorThing}>
                        <p className='glodzP'>{this.state.glodz}</p>
                    </ReactFitText>
                </div>
                <div className="glodzDiv vendorGlodzDiv">
                    <ReactFitText compressor={compressorThing}>
                        <p className='glodzP'>{this.state.vendorGlodz}</p>
                    </ReactFitText>
                </div>
            </div>
        );
    }
}

export default ShopInventory;