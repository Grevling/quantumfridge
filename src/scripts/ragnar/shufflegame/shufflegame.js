import React from "react";
import * as Utils from "../../utils";
import Sound from 'react-sound';

class SuffleGame extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            vibrasjonar: false,
            finished: false,
            goalState: Utils.getArrayOfConsecutiveIntegersFromOneTo(9),
            // positions: [1, 6, 2, 4, 5, 7, 3, 9, 8], //position of tiles; index 4 represents the empty space
            positions: this.getRandomPositions(), //position of tiles; index 4 represents the empty space
        };
    }

    checkIfFinished(){
        let finished = true;
        let positions = this.state.positions;
        let goalState = this.state.goalState;
        for (let i = 0; i < positions.length; i++) {
            if(!(positions[i] === goalState[i])) {
                finished = false;
            }
        }
        if (finished) {
            this.finished();
        }
    }

    finished(){
        this.setState({finished: true});
        this.props.resurrect();
    }

    getRandomPositions(){
        let positions = Utils.getArrayOfConsecutiveIntegersFromOneTo(9);
        for (let i = positions.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [positions[i], positions[j]] = [positions[j], positions[i]];
        }

        // positions = [1, 2, 3, 4, 8, 6, 7, 5, 9]; // one move from done, yay for testing!

        return positions;
    }

    shuffle(i, click) { // attempt to shuffle the tile with position of index i in the positions array
        if (i === 4 && click) {
            this.skullClick();
        }
        if(!this.state.finished) {
            let positions = this.state.positions;
            let clickedTileOldPosition = positions[i];
            let oldEmptyPosition = positions[4];
            let diff = clickedTileOldPosition - oldEmptyPosition;

            // examine the attempted shuffle:
            let horizontalMovement = Math.abs(diff) === 1;
            let verticalMovement = Math.abs(diff) === 3;
            let overFlowIfHorizontal = !(Math.trunc((clickedTileOldPosition - 1)/3) === Math.trunc((oldEmptyPosition - 1)/3));

            if (verticalMovement || (horizontalMovement && !overFlowIfHorizontal)) {
                [positions[i], positions[4]] = [oldEmptyPosition, clickedTileOldPosition];
                this.setState({
                    positions: positions,
                });
            }
            this.checkIfFinished();
        }
    }

    getTiles(){
        let returnArray = [];
        let positions = this.state.positions;
        let i = 9;
        while (i--){

            let className = 'shuffleTile tile' + i + ' pos' + positions[i];
            if (i === 4) {
                if (this.state.vibrasjonar) {
                    className += ' vibrasjonar';
                }
                if (this.state.finished) {
                    className += ' finished';
                }
            }
            let iCopy = i;
            if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                returnArray[i] = <div key={i} className={className} onClick={() => this.shuffle(iCopy, true)}/>;
            }else{
                returnArray[i] = <div key={i} className={className} onClick={() => this.shuffle(iCopy, true)} onMouseOver={() => this.shuffle(iCopy)}/>;
            }
        }
        return returnArray;
    }

    skullClick(){
        this.setState({vibrasjonar: !this.state.vibrasjonar});
    }


    render() {
        let tiles = this.getTiles();
        return (
            <div className='shuffleGameContent'>
                <Sound
                    url = {Utils.noiseUrls.elektronik}
                    playStatus={this.state.vibrasjonar ? Sound.status.PLAYING : Sound.status.PAUSED}
                />
                <div className='shuffleGameBackground' onClick={() => this.props.stopShuffling()}/>
                <div className='shuffleGameBoard'>
                    {tiles}
                </div>
            </div>
        );
    }
}

export default SuffleGame;