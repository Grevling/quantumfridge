import React from "react";
import ShopInventory from './shopInventory';
import '../../style/Dungeoneer/shop.css';

class Shop extends React.Component {
    constructor(props) {
        super(props);
        this.state = this.props.state
        // this.setState({shopping: false});
    }

    componentDidMount(){
        // console.log('didmount');
        this.setState({shopping: false});
    }

    render(){
        // console.log(this.state.shopping);
        let shopping = this.props.state.shopping;
        return(
            <div className='shopContent'>
                <div className='genericExitDivLeft' onClick={() => this.props.move('town')}/>
                {shopping ?  <div className='shopKeep busy' onClick={() => this.props.saveState({shopping: !shopping})}/> : <div className='shopKeep' onClick={() => this.props.saveState({shopping: !shopping})}/>}
                {shopping ?  <ShopInventory saveState={this.props.saveState}  state={this.props.state}/> : <div className='shoppingRagnar' onClick={() => vibratePattern()}/>}
            </div>
        );
    }
}

function vibratePattern() {
    navigator.vibrate(getMarch());
}

function getMarch(){
    return [50, 450,
        50, 200,
        50, 50,
        50, 50,
        50, 50,
        50, 200,
        50, 50,
        50, 50,
        50, 50,
        50, 50,
        50, 50,
        50, 50,
        50, 200,

        50, 600,
        50, 200,
        50, 50,
        50, 50,
        50, 50,
        50, 200,
        50, 50,
        50, 50,
        50, 50,
        50, 50,
        50, 50,
        50, 50,
        50, 200,
    ];
}

export default Shop;