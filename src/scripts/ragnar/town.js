import React from "react";
// import {Button, Clearfix} from 'react-bootstrap';
import '../../style/Dungeoneer/world.css';
import '../../style/Dungeoneer/town.css';
import * as Utils from "../utils";

class Town extends React.Component {
    constructor(props) {
        super(props);
        this.state = this.props.state

    }

    componentWillMount() {
        // let parentState = this.props.state;
        // this.setState(parentState);
    }

    resurrect(){}


    clearCookie(){
        Utils.deleteCookie('state');
        window.location.reload();
    }

    render(){

        return(
            <div className='townContent'>
                <h1 onClick={() => this.clearCookie()}>Tabula Rasa</h1>
                <div className="shop" onClick={() => this.props.move('shop')}/>
                <div className="churchExteriorDiv" onClick={() => this.props.move('church')}/>
                <div className='dungeonPath' onClick={() => this.props.move('dungeon')}/>
                <div className="dungeonPathDiv"/>
                <div className="clipPathDiv" onClick={() => this.props.move('clippath')}/>
            </div>
        );
    }
}

export default Town;