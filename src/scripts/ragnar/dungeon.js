import React from "react";
// import ReactFitText from 'react-fittext';
import {Clearfix} from 'react-bootstrap';
import ReactInterval from 'react-interval';
import InfoBox from './dungeonModules/infoBox'
import Resources from './dungeonModules/resources'
import * as Utils from '../utils';
import '../../style/Dungeoneer/dungeon.css';

class Dungeon extends React.Component {
    constructor(props) {
        super(props);
        this.shoot = this.shoot.bind(this);
        this.tranq = this.tranq.bind(this);
        this.usePotion = this.usePotion.bind(this);
        this.tick = this.tick.bind(this);
        this.getState = this.getState.bind(this);
        // this.state = {
        // };
    }

        componentWillMount() {
        let newState = this.props.state;
        this.setState(newState);
    }

    componentDidMount(){
        this.updateHealth();
    }

    updateHealth(){
        let damageTaken = this.state.ragnarMaxHealth - this.state.ragnarHealth;
        let healthFromShields = this.state.ragnarsPocket.shields * this.state.shieldHealthIncrease;
        let otherHealthBonuses = 0; // assume there will be other health boost sources
        let newMaxHealth = 100 + healthFromShields + otherHealthBonuses;
        let newHealth = newMaxHealth - damageTaken;
        if(newHealth <= 0) {
            this.saveState({
                dead: true,
                ragnarMaxHealth: newMaxHealth,
                ragnarHealth: 0
            });
            this.info("Ragnar has died from changing his clothes. Well done Ragnar!");
        }else{
            this.saveState({
                ragnarMaxHealth: newMaxHealth,
                ragnarHealth: newHealth
            });
        }

    }

    getState(){
        return this.state;
    }

    saveState(state){
        this.setState(state);
        // console.log('saaving state in dungeon');
        this.props.saveState(state);
    }

    move(destination){
        this.props.move(destination);
    }

    tick(){
            this.handleTranqStates();
    };

    handleTranqStates(){
        //todo make separate Date based tranq debuffs
        let ragnarTranqTimer = this.state.ragnarTranqTimer;
        let ragnarSlowAmount = this.state.ragnarSlowAmount;
        if (ragnarTranqTimer > 0) {
            if (ragnarTranqTimer === 1) {
                ragnarSlowAmount = 0;
            }
            ragnarTranqTimer--;
        }
        let monsterTranqTimer = this.state.activeMonster.tranqTimer;
        let monsterSlowAmount = this.state.activeMonster.slowAmount;
        if (monsterTranqTimer > 0) {
            if (monsterTranqTimer === 1) {
                monsterSlowAmount = 0;
            }
            monsterTranqTimer--;
        }
        this.saveState({
            ragnarTranqTimer: ragnarTranqTimer,
            ragnarSlowAmount: ragnarSlowAmount,
            monsterTranqTimer: monsterTranqTimer,
            monsterSlowAmount: monsterSlowAmount
        })
    }

    usePotion(index){
        if(this.state.dead){
            this.deadMessage();
        }else {
            let potionArray = this.state.healingPotions;
            let oldValue = potionArray[index];
            if (oldValue === 1) { //if the chosen potion is full
                let potionRechargeStartTime = this.state.potionRechargeStartTime;
                if (potionRechargeStartTime === null) {
                    potionRechargeStartTime = Date.now();
                }
                navigator.vibrate([50, 10, 40, 10, 30, 10, 20, 10, 10]);
                this.healRagnar(this.state.potionPotency);
                potionArray[index] = 0;
                this.saveState({
                    potionRechargeStartTime: potionRechargeStartTime,
                    healingPotions: potionArray,
                });
            }
        }
    }

    info(text){
        // console.log('info: ' + text);
        this.saveState({
            infoText: text
        });
    }

    loot(lootz){
        this.saveState({
            glodz: this.state.glodz + lootz
        });
    }

    
    
    newMonster(){
        let monsters = this.state.monsters;
        let monster = monsters[this.state.activeMonsterId];
        monster.health = monster.maxHealth;
        monster.tranqTimer = 0;
        monster.slowAmount = 0;
        let nextMonsternumber = Utils.getRandomInt(0, monsters.length - 1);

        this.saveState({
            fighting: false,
            monsterDead: true,
            activeMonster: monsters[nextMonsternumber],
            activeMonsterId: nextMonsternumber,
            monsters: monsters
        });
        this.animateRespawn();
    }

    glodValue(glod){
        if(glod > 1) {
            return '' + glod + ' glodz';
        } else {
            return '' + glod + ' glod';
        }
    }

    victory(){
        // this.setState({monsterDead: true});
        let loot = this.state.activeMonster.loot + this.state.ragnarsPocket.rings;
        this.info(`Ragnar has vanquished ${this.state.activeMonster} and found ${this.glodValue(loot)} worth of fat loot. But what is that? Another monster, just around the bend!`);
        this.loot(loot);
        this.newMonster();
    };

    gitDedd(){
        this.saveState({fighting: false, dead: true});
        this.deadMessage();
    };

    deadMessage(){
        this.info('Ragnar is dead. Maybe someone in town can put him back together? ' + this.state.ragnarHealth);
    }

    ragnarAttack(){
        if(this.state.fighting) {
            // console.log('Ragnar Attacks!');
            this.attackAnimation(true);
            this.hurtMonster(5 + this.state.ragnarsPocket.swords);
        }
    }

    monsterAttack(){
        if(this.state.fighting) {
            // console.log('Monster Attacks!');
            this.attackAnimation(false);
            this.hurtRagnar(5);
        }
    }

    healRagnar(number){
        this.hurtRagnar(-number);
    }

    hurtRagnar(number){
        let oldHealth = this.state.ragnarHealth;
        //todo use shield
        let newHealth = oldHealth - number;
        if(newHealth > this.state.ragnarMaxHealth) {
            newHealth = this.state.ragnarMaxHealth;
        }
        if(newHealth <= 0) {
            this.gitDedd();
            newHealth = 0;
        }
        this.saveState({
            ragnarHealth: newHealth
        });
    }

    hurtMonster(number){
        let monsters = this.state.monsters;
        let monster = monsters[this.state.activeMonsterId];
        let oldHealth = monster.health;
        monster.health = oldHealth - number;
        this.saveState({
            monsters: monsters
        });
        if(monster.health <= 0) {
            this.victory();
        }
    }

    startFight(){
        if (!this.state.dead) {
            this.saveState({fighting: true});
        }
        // this.saveState({
        //     fighting: true
        // });
    }



    tranqMonster(){
        this.saveState({
            monsterSlowAmount: this.state.activeMonster.slowAmount + 1,
            monsterTranqTimer: 10,
        })
    }

    tranqRagnar(){
        this.saveState({
            ragnarSlowAmount: this.state.ragnarSlowAmount + 1,
            ragnarTranqTimer: 10,
        })}

    tranq(index){
        if(!this.state.dead){
            let tranqArray = this.state.tranqDarts;
            let oldValue = tranqArray[index];
            if (oldValue === 0) {
                this.info('That dart is a future dart,try launching it in the future.')
            } else {
                this.shoot(true);
                tranqArray[index] = 0;
                this.saveState({
                    tranqDarts: tranqArray,
                });
            }
        }
    }


    attackAnimation(ragnar){
        // console.log('doing attack animation');
        this.queueAttackFrame(ragnar, 1,  20);
        this.queueAttackFrame(ragnar, 2, 40);
        this.queueAttackFrame(ragnar, 3, 60);
        this.queueAttackFrame(ragnar, 4, 80);
        this.queueAttackFrame(ragnar, 3, 100);
        this.queueAttackFrame(ragnar, 2, 120);
        this.queueAttackFrame(ragnar, 1,  140);
        this.queueAttackFrame(ragnar, 0, 160);
    }


    animateRespawn(){
        let respawnTime = 1000;
        let respawnFrames = 10;
        let interval = respawnTime/respawnFrames;
        let i = respawnFrames;
        while (i--){
            this.queueRespawnFrame(i*20, (respawnFrames-i)*interval);
            // respawnTime = i*200;
        }

        setTimeout(function() {
            this.setState({monsterDead: false});
        }.bind(this), respawnTime);
    }

    queueAttackFrame(ragnar, frame, time){
        setTimeout(function() {
            if(ragnar) {
                this.setState({ragnarAttackAnimationFrame: frame});
            }else{
                this.setState({monsterAttackAnimationFrame: frame});
            }
        }.bind(this), time);
    }

    queueRespawnFrame(frame, time){
        // console.log('queuing respawn frame > frame: ' + frame + ' time: ' + time);
        setTimeout(function() {
            this.setState({monsterRespawnFrame:frame});
        }.bind(this), time);
    }



    shoot(tranq){
        console.log(tranq);
        if(this.state.dead){
            this.deadMessage();
        } else if(!this.state.monsterDead) {
            console.log(this.state.monsterDead);
            // this.animateBow();
            this.startFight();
            // console.log('animateBow!');
            let accuracy = this.state.bowAccuracy;
            let shootyPewPew = Math.random() * 100;
            if (accuracy >= shootyPewPew) {
                this.info(`You shot ${this.state.activeMonster.name}.`);
                this.hurtMonster(this.state.bowDamage);
                if (tranq) {
                    this.tranqMonster();
                    this.info(`Your tranq dart hit ${this.state.activeMonster.name}; its movements become more sluggish.`);
                }
            } else if ((accuracy + (100 - accuracy) / 2) >= shootyPewPew) {
                this.info(`You shot Ragnar. Why did you shoot Ragnar?`);
                this.hurtRagnar(this.state.bowDamage);
                if (tranq) {
                    this.tranqRagnar();
                    this.info(`You just tranqt Ragnar. That seems highly counterproductive. He appears to enjoy the buzz but still.`);
                }
            } else {
                this.info(`You missed.`);
            }
        }
    }



    getMonsterAttackInterval(){
        let monsterAttackSpeed = this.state.activeMonster.attackSpeed;
        let monsterSlowAmount = this.state.activeMonster.slowAmount;
        if(monsterSlowAmount > 0){
            monsterAttackSpeed = monsterAttackSpeed -(monsterSlowAmount/10)
        }
        // console.log('returning attackspeed: ' + monsterAttackSpeed);
        return 1000 / monsterAttackSpeed;
    }

    getRagnarAttackInterval(){
        let ragnarAttackSpeed = this.state.ragnarAttackSpeed + (0.1 * this.state.ragnarsPocket.gloves);
        let ragnarSlowAmount = this.state.ragnarSlowAmount;
        if(ragnarSlowAmount > 0){
            ragnarAttackSpeed = ragnarAttackSpeed -(ragnarSlowAmount/10)
        }
        // console.log('returning attackspeed: ' + monsterAttackSpeed);
        return 1000 / ragnarAttackSpeed;
    }

    getRagnarState(){
        if(this.state.dead) {
            return 'ragnarPicture dead';
        }else if(this.state.ragnarHealth < (this.state.ragnarMaxHealth*0.5)) {
            return 'ragnarPicture hurt';
        }
        return 'ragnarPicture';
    }

    render(){
        // debugger;
        let ragHealthStyle = {width: Utils.percentageString(this.state.ragnarHealth, this.state.ragnarMaxHealth)};
        let monHealthStyle = {width: Utils.percentageString(this.state.activeMonster.health, this.state.activeMonster.maxHealth)};
        let ragnarAttackTimer = <ReactInterval timeout={this.getRagnarAttackInterval()} enabled={true} callback={() => this.ragnarAttack()} />;
        let monsterAttackTimer = <ReactInterval timeout={this.getMonsterAttackInterval()} enabled={true} callback={() => this.monsterAttack()} />;
        let hideIfFightingStyle = this.state.fighting ? {display: 'none'} : {};
        let ragnarClass = this.getRagnarState() + ' right' + this.state.ragnarAttackAnimationFrame;
        let monsterPositionStyle = {transform: "translateX("+ this.state.monsterRespawnFrame+"%)"};
        let monsterModel = this.state.activeMonster.name;
        // console.log(monsterModel);

        // let bowClass = this.state.animateBow ? "bowDiv shooty" : "bowDiv";
        return(
            <div id='dungeon'>
                <div className="faceDiv"/>
                <div className='dungeonExitDiv' style={hideIfFightingStyle} onClick={() => {this.saveState({monsterDead: false}); this.move('town')}}/>
                <div id="battleGround">
                    <div id='timerDiv'>
                        <ReactInterval timeout={1000} enabled={true}
                                       callback={() => this.tick()} />
                        {ragnarAttackTimer}
                        {monsterAttackTimer}
                    </div>
                    <Clearfix/>
                    <InfoBox state={this.state} />
                    <div id="ragnar">
                        <div className='healthBar' id='ragnarHealthBar'>
                            <div className='health' id='ragnarHealth' style={ragHealthStyle}/>
                        </div>
                        <div className={ragnarClass}/>
                    </div>
                    <div id="monster" style={monsterPositionStyle} className={this.state.fighting || this.state.dead ? 'noPointer ' : 'pointer ' + ({monsterModel})}  onClick={() => this.startFight()}>
                        <div className='healthBar' id='monsters[this.state.activeMonsterId].healthBar'>
                            <div className='health' id='monsters[this.state.activeMonsterId].health' style={monHealthStyle}/>
                        </div>
                        <div className={(this.state.dead ? "gone" : "monsterPicture left") + this.state.monsterAttackAnimationFrame + " model" + this.state.activeMonsterId}/>
                    </div>
                </div>
                <Resources getState={this.getState} state={this.state} shoot={this.shoot} saveState={this.props.saveState} usePotion={this.usePotion} tick={this.tick}/>
            </div>
        );
    }
}

export default Dungeon;