import React from "react";
import Town from './town';
import Dungeon from './dungeon';
import Church from './church';
import Shop from './shop';
import {Button} from 'react-bootstrap';
import '../../style/Dungeoneer/world.css';
import * as Utils from "../utils";
import ClipPathPicture from "./clippath";

class World extends React.Component {
    constructor() {
        super();
        this.state = getDefaultState();
        this.move = this.move.bind(this);
        this.saveState = this.saveState.bind(this);
    }


    componentWillMount(){
        // debugger;
        if (this.state.activeMonster === undefined) {
            let nextMonsterNumber = Utils.getRandomInt(0, this.state.monsters.length);

            this.saveState({
                activeMonsterId: nextMonsterNumber,
                activeMonster: this.state.monsters[nextMonsterNumber],
            });
            // this.newMonster();
        }
        this.getCookieState();
    }

    getCookieState(){
        try {
            let cookieStateString = Utils.getCookie('state');
            let stateObject = JSON.parse(cookieStateString);
            this.setState(stateObject);
        }
        catch(err) {
            console.log('FAILED TO LOAD STATE FROM COOKIES.');
        }
    }


    saveState(state){
            this.setState(state);
        // let stateString = JSON.stringify(this.state);
        // Utils.setCookie('state', stateString, 99);
    }
    
    move(location){
        console.log('moving ' + location);
        this.saveState({location: location})
    }

    render(){
        let stateString = JSON.stringify(this.state);
        Utils.setCookie('state', stateString, 99);
        let content = <Town state={this.state} move={this.move} />;

        if (this.state.location === 'dungeon') {
            content = <Dungeon saveState={this.saveState} state={this.state} move={this.move}/>;
        }else if (this.state.location === 'church') {
            content = <Church saveState={this.saveState} state={this.state} move={this.move}/>;
        }else if (this.state.location === 'shop') {
            content = <Shop saveState={this.saveState} state={this.state} move={this.move}/>;
        }else if (this.state.location === 'clippath') {
            content = <ClipPathPicture saveState={this.saveState} state={this.state} move={this.move}/>;
        }

        return(
            <div id='worldContent'>
                <div className='topThingDiv'>
                    <Button bsStyle='danger' className='goToMenuButton worldGoToMenuButton' //todo reclassify buttons
                            onClick={() => this.props.returnmethod('menu')}>{'Back To Menu'}</Button>
                </div>
                {content}
            </div>
        );
    }
}
export default World;

function getDefaultState(){
    return {
        sliderSpot: 1,
        vendorGlodz: 13,
        location: 'town',
        dead: false,
        glodz: 0,
        infoText: 'You follow Ragnar the Dungeoneer into the scary dungeon where a monster lurks maliciously.',
        ragnarHealth: 100,
        ragnarMaxHealth: 100,
        fighting: false,

        bowAccuracy: 80,
        bowDamage: 1,
        bowFrame: 1,

        monsterRespawnFrame: 0,
        monsterDead: false,

        ragnarAttackSpeed: 1.2,
        ragnarBaseAttackSpeed: 1.2,
        ragnarSlowAmount: 0,
        ragnarTranqTimer: 0,

        ragnarAttackAnimationFrame: 0,

        activeMonsterId: 0,

        monsters: [{
            name: 'Jeff',
            loot: 2,
            health: 150,
            maxHealth: 150,
            attackSpeed: 1,
            baseAttackSpeed: 1,
            slowAmount: 0,
            tranqTimer: 0,
            attackAnimationFrame: 0,
        },{
            name: 'Goblin "King"',
            loot: 2,
            health: 100,
            maxHealth: 100,
            attackSpeed: 0.8,
            baseAttackSpeed: 0.8,
            slowAmount: 0,
            tranqTimer: 0,
            attackAnimationFrame: 0,
        },{
            name: 'Dragon',
            loot: 1,
            health: 20,
            maxHealth: 20,
            attackSpeed: 2,
            baseAttackSpeed: 2,
            slowAmount: 0,
            tranqTimer: 0,
            attackAnimationFrame: 0,
        },
            ],

        potionPotency: 25,
        healingPotions: [1,1,0],
        potionRechargeTime: 10000,
        potionRechargeStartTime: null,
        potionCharge: 0,

        tranqDarts: [1,1],
        tranqRechargeTime: 10000,
        tranqRechargeStartTime: null,
        tranqCharge: 0,

        shieldHealthIncrease: 2,

        ragnarsPocket:{
            swords: 0,
            shields: 0,
            rings: 0,
            boots: 0,
            gloves: 0,
        },
        items : [
            {
                id:1,
                name: 'Sword of Hurting',
                price: 10,
                type: 'sword'
            },
            {
                id:2,
                name: 'Shield of Protecting',
                price: 10,
                type: 'shield'
            },
            {
                id:3,
                name: 'Ring of Bling',
                price: 15,
                type: 'ring'
            },
            {
                id:4,
                name: 'Boots of Feet',
                price: 10,
                type: 'boot'
            },
            {
                id:5,
                name: 'Gloves of Gripping',
                price: 10,
                type: 'glove'
            }
        ],
        epix: [ {
            name: 'Handle',
            description: 'Part of what was once probably an exceptionally mediocre weapon.',
            price: 10,
        },{
            name: 'Holy Shoe',
            description: "There's only one. Plenty of holes though!",
            price: 9,
        },{
            name: 'Probably Fish',
            description: 'With approximately 78% certainty. What else could generate that smell?',
            price: -3,
        },{
            name: 'Portable Mountain',
            description: 'A tiny uprooted mountain. Not a pebble. Mountain.',
            price: 1000,
        }
        ],

    };
}
