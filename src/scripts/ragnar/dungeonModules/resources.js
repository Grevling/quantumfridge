import React from 'react';
import ReactInterval from 'react-interval';
import * as Utils from "../../utils";

class Resources extends React.Component{
    constructor(props){
        super(props);
        this.state = this.props.state;
    }

    saveState(state){
        this.setState(state);
        this.props.saveState(state);
    }

    tick(){
        this.props.tick();
        if(!this.props.state.dead) {
            this.updateInventory();
        }
    }

    handleRegen(tranqs){
        let state = this.state;
        let thingsString = tranqs ? 'tranqDarts' : 'healingPotions';
        let rechargeTimeString = tranqs ? 'tranqRechargeTime' : 'potionRechargeTime';
        let startTimeString = tranqs ? 'tranqRechargeStartTime' : 'potionRechargeStartTime';
        let chargeString = tranqs ? 'tranqCharge' : 'potionCharge';

        let things = state[thingsString];
        let thingsSum = things.reduce((a, b) => a + b, 0);
        let missingThings = things.length - thingsSum;

        if (missingThings > 0) {
            let rechargeStartTime = state[startTimeString]; // when we started recharging this thing
            if (rechargeStartTime === null) {
                let stateObjectZero = {};
                stateObjectZero[startTimeString] = Date.now();
                this.saveState(stateObjectZero);
            } else { // if rechargeStartTime was not null
                let now = Date.now();
                let howLongBeenChargingThisThing = now -  rechargeStartTime; // how long ago we started recharging this thing
                let howLongToCharge = state[rechargeTimeString]; // how long it takes to recharge a thing

                if(howLongBeenChargingThisThing < howLongToCharge){// if we're not done recharging this thing
                    let stateObjectOne = {};
                    stateObjectOne[chargeString] = (howLongBeenChargingThisThing / howLongToCharge) * howLongToCharge; // set current charge amount
                    this.saveState(stateObjectOne);
                }else{
                    tranqs ? this.addTranqDart() : this.addPotion();
                    let newRechargeStartTime = (missingThings > 1) ? (rechargeStartTime + howLongToCharge) : null; // if there are more things to recharge, create a timeStamp for for when started recharging new thing
                    let stateObjectTwo = {};
                    stateObjectTwo[startTimeString] = newRechargeStartTime;
                    this.saveState(stateObjectTwo);
                }

            }
        }
    }

    updateInventory(){
        this.handleRegen(false);
        this.handleRegen(true);
    }

    addTranqDart(){
        let tranqArray = this.state.tranqDarts;
        let firstEmptyIndex = tranqArray.indexOf(0);
        tranqArray[firstEmptyIndex] = 1;

        this.saveState({
            tranqDarts:  tranqArray,
            tranqCharge: 0
        });
    }

    addPotion(){
        let potionArray = this.state.healingPotions;
        let firstEmptyIndex = potionArray.indexOf(0);
        potionArray[firstEmptyIndex] = 1;

        this.saveState({
            healingPotions:  potionArray,
            potionCharge: 0
        });
    }


    getPotions(){
        let potionArray = this.state.healingPotions;
        let potionDivs = [];

        for(let i = 0; i < potionArray.length; i++){
            let idString = 'potion' + (i + 1) + 'Div';
            potionDivs[i] = <div className={'potionDiv ' + this.getPotionClass(i)} id={idString} onClick={() => this.props.usePotion(i)} key={i}/>
        }
        return potionDivs;
    }


    getPotionClass(index){
        let potionArray = this.state.healingPotions;
        if(potionArray[index] === 1) {
            return 'pointer';
        }else {
            return 'emptyPotionDiv';
        }
    }



    getTranqs(){
        let tranqArray = this.state.tranqDarts;
        let tranqDivs = [];

        for(let i = 0; i < tranqArray.length; i++){
            let idString = 'tranq' + (i + 1) + 'Div';
            tranqDivs[i] = <div className={'tranqDiv ' + this.getTranqClass(i)} id={idString} onClick={() => this.tranq(i)}  key={i}/>
        }
        return tranqDivs;
    }

    getTranqClass(index){
        let potionArray = this.state.tranqDarts;
        if(potionArray[index] === 1) {
            return 'readyTranqDiv';
        }else {
            return 'spentTranqDiv';
        }
    }


    animateBow(){
        this.queueBowFrame(2, 50);
        this.queueBowFrame(3, 100);
        this.queueBowFrame(4, 150);
        this.queueBowFrame(1, 200);
    }


    queueBowFrame(frame, time){
        setTimeout(function() {
            this.setState({bowFrame:frame});
        }.bind(this), time);
    }


    tranq(index){
        console.log('tranq1');
        if (!this.state.dead && !this.state.monsterDead) {
            let tranqArray = this.state.tranqDarts;
            let oldValue = tranqArray[index];
            if (oldValue === 0) {
                console.log('That dart is a future dart,try launching it in the future.');
                this.saveState({
                    infoText: 'That dart is a future dart,try launching it in the future.'
                });
            } else {
                this.props.shoot(true);
                this.animateBow();
                navigator.vibrate([20, 10, 20, 10, 20]);
                tranqArray[index] = 0;
                this.props.saveState({
                    tranqDarts: tranqArray,
                });
            }
        }
    }

    shoot(){
        if (!this.props.getState().dead && !this.props.getState().monsterDead) {
            this.animateBow();
            navigator.vibrate(50);
            this.props.shoot();
        }
    }

    render(){
        let potionChargeStyle = {width: Utils.percentageString(this.state.potionCharge, this.state.potionRechargeTime)};
        let tranqChargeStyle = {width: Utils.percentageString(this.state.tranqCharge, this.state.tranqRechargeTime)};
        let bowClass = "bowDiv frame" + this.state.bowFrame;

        return (
            <div className='resourcesDiv'>
                <ReactInterval timeout={100} enabled={true}
                               callback={() => this.tick()} />
                <div className='potionsDiv'>
                    <div className='wideDiv'/>
                    <div className='narrowDiv'/>
                    {this.getPotions()}
                    <div className='chargeDiv potionChargeDiv'>
                        <div className='potionChargeValueDiv' style={potionChargeStyle}/>
                    </div>

                </div>
                <div className={bowClass} onClick={() => this.shoot()}/>
                <div className='tranqsDiv'>
                    <div className='narrowDiv'/>
                    {this.getTranqs()}
                    <div className='chargeDiv tranqChargeDiv'>
                        <div className='tranqChargeValueDiv' style={tranqChargeStyle}/>
                    </div>
                </div>
            </div>
        );
    }

}
export default Resources;