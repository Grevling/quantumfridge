import React from 'react';
import ReactFitText from 'react-fittext';

class InfoBox extends React.Component{
    render(){
        // console.log('rendering infotext');
        return (
            <div className='infoDiv'>
                <div className='statusDiv'>
                    <ReactFitText compressor={1}>
                        <p>Glodz: <span id='glodSpan'>{this.props.state.glodz}</span></p>
                    </ReactFitText>
                </div>
                <div className='announcementDiv'>
                    <ReactFitText compressor={2}>
                        <p>{this.props.state.infoText}</p>
                    </ReactFitText>
                </div>
            </div>
        );
    }

}
export default InfoBox;