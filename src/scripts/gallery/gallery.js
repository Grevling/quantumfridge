import React from "react";
import {Button} from 'react-bootstrap';

class Gallery extends React.Component {
    // constructor() {
    //     super();
    // }

    render(){
        return(
           <div className='galleryContent'>
               <Button bsStyle='danger' className='goToMenuButton' //todo reclassify buttons
                       onClick={() => this.props.returnmethod('menu')}>{'Back To Menu'}</Button>
               <div className="pictureDiv" id="oneCity"/>
           </div>
        );
    }
}
export default Gallery;
