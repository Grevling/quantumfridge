import React from "react";
import Noise from './noise';
import ReactFitText from 'react-fittext';
import * as Utils from "../utils";

class NoiseBox extends React.Component {
    constructor(props) {
        super(props);
        // window.onkeyup = function(e){console.log(e.code);};
        this.state = {
            playing: null,
            focusNoise: 0,
            noiseObjects: this.getNoiseObjects(),
        };
        this.myHandleKeyUp = this.myHandleKeyUp.bind(this);
        this.handleNoiseClick = this.handleNoiseClick.bind(this);
        this.handleNoiseDone = this.handleNoiseDone.bind(this);
    }

    myHandleKeyUp(e){
        // console.log(e.code);

        switch(e.code) {
            case 'Space':
                this.togglePlay();
                break;
            case 'ArrowUp':
                this.switchNoise(false);
                break;
            case 'ArrowLeft':
                this.switchNoise(false);
                break;
            case 'ArrowDown':
                this.switchNoise(true);
                break;
            case 'ArrowRight':
                this.switchNoise(true);
                break;
            default:
        }
    }
    
    getNoiseObjects(){
        return[
            {
                title: 'One City',
                urlKey: 'oneCity'
            },{
                title: 'Data Frame',
                urlKey: 'dataFrame'
            },{
                title: 'MTC',
                urlKey: 'mtc'
            },{
                title: 'Guatemalan Beeherders',
                urlKey: 'guatemalanBeeherders'
            },{
                title: 'Depression Of The Triangular Button',
                urlKey: 'depressionOfTheTriangularButton'
            },{
                title: 'p88',
                urlKey: 'p88'
            },
        ]
    }

    componentDidMount(){
        let keyUpHandler = this.myHandleKeyUp;
        window.onkeyup = function(e){
            keyUpHandler(e);
        };
    }

    togglePlay(){
        if(this.state.playing != null && this.state.playing === this.state.focusNoise) {
            console.log('stop!');
            this.setState({playing: null});
        }else{
            this.setState({playing: this.state.focusNoise});
        }
    }

    handleClick(e){
        e.target.blur();
        this.togglePlay();
    }

    handleNoiseDone(){
        this.setState({focusNoise: this.state.playing});
        this.switchNoise(true);
        this.togglePlay();
    }

    getNoises(){
        const noiseObjects = this.state.noiseObjects;
        let returnArray = [];
        for(let i = 0; i < noiseObjects.length; i++) {
            let obj = noiseObjects[i];
            returnArray.push(
                <Noise
                    key={i}
                    id={i}
                    focus={i === this.state.focusNoise}
                    playing={i === this.state.playing}
                    obj={obj}
                    onClick={this.handleNoiseClick}
                    done={this.handleNoiseDone}
                />
            );
        }
        return returnArray;
    }

    handleNoiseClick(id){
        this.setState({
            focusNoise: id
        });
    }

    switchNoise(down){
        const numberOfNoises = this.state.noiseObjects.length;
        let oldNoiseId = this.state.focusNoise;
        let newNoiseId;
        if(down) {
            newNoiseId = oldNoiseId + 1;
            if(newNoiseId >= numberOfNoises){
                newNoiseId = 0;
            }
        } else {
            newNoiseId = oldNoiseId - 1;
            if(newNoiseId < 0){
                newNoiseId = numberOfNoises - 1;
            }
        }
        this.setState({
            focusNoise: newNoiseId,
        });
    }

    render() {
        let mobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
        // mobile = true;
        return (
            <div className={'noiseBoxContent ' + (mobile ? 'mobile' : 'normal')}>
                <ReactFitText compressor={0.4}>
                    <p className={'playButton ' + (this.state.playing != null ? 'playing ' : 'paused ') + (mobile ? 'mobile' : 'normal')} onClick={(e) => this.handleClick(e)}/>
                </ReactFitText>
                {this.getNoises(this.state.playing)}
            </div>
        );
    }
}

export default NoiseBox;