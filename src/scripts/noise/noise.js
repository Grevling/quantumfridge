import React from "react";
import Sound from 'react-sound';
import ReactFitText from 'react-fittext';
import * as Utils from "../utils";

class Noise extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let titleLength = this.props.obj.title.length > 13 ? 'long' : 'short';
        let className = 'noise ' + (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ? 'mobile' : 'normal');
        if(this.props.playing){ className += ' playing'; }
        else if (this.props.focus){ className += ' focus'; }

        let comp = 0.8;
        if(this.props.obj.title.length > 23) {
            comp = 1.1;
            titleLength = 'veryLong';
        }

        return (
            <div className={className} onClick={() => this.props.onClick(this.props.id)}>
                <ReactFitText compressor={comp}>
                    <h1 className={'noiseTitle ' + titleLength}>{this.props.obj.title}</h1>
                </ReactFitText>
                <Sound
                    url={Utils.noiseUrls[this.props.obj.urlKey]}
                    playStatus={this.props.playing ? Sound.status.PLAYING : Sound.status.PAUSED}
                    onFinishedPlaying={() => this.props.done()}
                />
            </div>
        );
    }
}

export default Noise;