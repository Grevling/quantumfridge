import React, { Component } from 'react';
import Pattern from "./scripts/patterns/pattern";
import Game from './scripts/tictactoe/game';
import Gallery from './scripts/gallery/gallery';
import World from './scripts/ragnar/world';
import NoiseBox from './scripts/noise/noiseBox';

import './style/pattern.css';
import './style/tictactoe.css';
import './style/menuThing.css';
import './style/noiseBox.css';
import './App.css';

/* global soundManager:false */
import 'react-sound';
soundManager.setup({debugMode: false});

class App extends Component {
    constructor(){
        super();
        this.state = {
            mode: "menu",
            height: 0,
            width: 0,
            repoUrl: 'https://bitbucket.org/Grevling/quantumfridge/src'
        };
        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
        this.setMode = this.setMode.bind(this);
    }


    componentDidMount() {
        this.updateWindowDimensions();
        window.addEventListener('resize', this.updateWindowDimensions);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
    }

    updateWindowDimensions() {
        //todo make this redraw pattern square if it exists
        this.setState({ width: window.outerWidth, height: window.outerHeight });
    }


    getMenu(){
        return (
            <div id="menuDiv" className="capture" ref="Progress1">
                <div id="dungeoneerDiv" className="menuItem" onClick={() => this.setMode("ragnar")}/>
                <div id="gameDiv" className="menuItem" onClick={() => this.setMode("game")}/>
                <div id="patternDiv" className="menuItem" onClick={() => this.setMode("pattern")}/>
                <div id="galleryDiv" className="menuItem" onClick={() => this.setMode("gallery")}/>
            </div>
        );
    }

    setMode(mode){
        // console.log('setMode called');
        this.setState({
            mode: mode
        })
    }

    render() {
        var mode = this.state.mode;
        // let renderButton = !(mode == "menu");
        var thingy = this.getMenu();
        if(mode === "game") { thingy = <Game returnmethod = {this.setMode}/> }
        else if(mode === "pattern") { thingy = <Pattern returnmethod = {this.setMode}/> }
        else if(mode === "gallery") { thingy = <Gallery returnmethod = {this.setMode}/> }
        else if(mode === "ragnar") { thingy = <World returnmethod = {this.setMode}/> }

        // thingy = <World returnmethod = {this.setMode}/>;

        let sideLength = Math.min(this.state.height*0.8, this.state.width*0.9);
        // console.log(sideLength);
        let sideString = "" + sideLength + "px";



        let wrapperStyle = {
            // margin: "3em auto 0 auto",
            width: sideString,
            paddingBottom: sideString,
            position: "relative"
        };

        let mobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);

        return (
            <div className='wrapper unselectable' style={wrapperStyle}>
                <div className={'content ' + (mobile ? 'mobile' : 'normal')}>
                    <NoiseBox id='noiseBox'/>
                    {/*<Button bsStyle='primary' className='modeButton' onClick={() => this.switchMode()}>{buttonText}</Button>*/}
                    {/*{this.getMenuButton(renderButton)}*/}
                    {thingy}
                </div>
            </div>

        );

    }
}

export default App;
